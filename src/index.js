// src/index.js

function soma(n1, n2) {
    return n1 + n2;
}

function subtrai(n1, n2) {
    return n1 - n2;
}

module.exports = {
    soma,
    subtrai
}
